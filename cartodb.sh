#!/bin/bash

PROGNAME=${0##*/}

do_help()
{
  printf "
Usage:
    $PROGNAME [options] {start|stop|restart}

Options:

    -m, --start-mode MODE
        set start mode, 'p' (production) 'd' (development), default development

    -h, --help
        output help message

"
}

SHORTOPTS="hm:"
LONGOPTS="help,start-mode:"

TMP=$(mktemp)
ARGS=$(getopt -s bash --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" 2>"$TMP")
ERR=$(cat "$TMP")
rm "$TMP"

if [ -n "$ERR" ]; then echo; echo "$ERR"; do_help; exit 1; fi

eval set -- "$ARGS"

while true
do
    case "$1" in
        -h|--help)
            do_help
            exit 0
            ;;
        -m|--start-mode)
            case "$2" in
                "") shift 2 ;;
                *) START_MODE="$2"; shift 2 ;;
            esac
            ;;
        --)
            shift
            break
            ;;
        *)
            error_exit 'Internal error!'
            ;;
    esac
done

if [ $# -eq 0 ]; then
   echo 'start/stop/restart required!'
   exit 1
fi

source $HOME/.rvm/scripts/rvm

COMMAND="$1"

if [ "${START_MODE,,}" == 'p' ]
then
  START_MODE=production
else
  START_MODE=development
fi

SQL_API_PID_FILE=/var/lib/cartodb/sql_api.pid
TILER_PID_FILE=/var/lib/cartodb/tiler.pid

if [ "${COMMAND,,}" == 'stop' -o "${COMMAND,,}" == 'restart' ]
then
    echo "--- Stopping CartoDB ($START_MODE) ---"
    if [ "$START_MODE" == 'production' ]
    then
        sudo killall nginx
    else
        SRV_PID=$(ps -Ao pid,cmd | grep -E '.*[/]ruby.*rails server' | awk '{ print $1; }')
        kill $SRV_PID
    fi
    SQL_API_PID=$(cat $SQL_API_PID_FILE 2>/dev/null)
    TILER_PID=$(cat $TILER_PID_FILE 2>/dev/null)
    if [ -n "$SQL_API_PID" ]; then kill "$SQL_API_PID"; rm $SQL_API_PID_FILE 2>/dev/null; fi
    if [ -n "$TILER_PID" ]; then kill "$TILER_PID"; rm $TILER_PID_FILE 2>/dev/null; fi
    RSQ_PID=$(ps -Ao pid,cmd | grep -E 'resque[-]' | awk '{ print $1; }')
    kill $RSQ_PID
fi

if [ "${COMMAND,,}" == 'start' -o "${COMMAND,,}" == 'restart' ]
then
    echo "--- Starting CartoDB ($START_MODE) ---"
    if [ "$START_MODE" == 'production' ]
    then
        export RAILS_ENV=production
        rvm use 1.9.3@cartodb --default
    else
        export RAILS_ENV=development
        rvm use 1.9.3@cartodb-dev --default
    fi
    BACKGROUND=yes QUEUE=* bundle exec rake resque:work
    if [ "$START_MODE" == 'production' ]
    then
        sudo /opt/nginx/sbin/nginx
    else
        bundle exec rails server -p 3000 -d
    fi
    node ../CartoDB-SQL-API/app.js $START_MODE >/dev/null &
    echo $! > $SQL_API_PID_FILE
    node ../Windshaft-cartodb/app.js $START_MODE >/dev/null &
    echo $! > $TILER_PID_FILE
fi

