## Addtitional things to do ##

### To make markers upload possible: ###
* create an AWS S3 bucket 
* set your S3 credentials and this bucket name in cartodb/config/app_config.yml
* restart

### Markersnot displaying in shared maps (key functionality) ###




##Softevol##
At first, you should remember that sources provided on Github seems not was supposed to use in Production mode.
We have changed much of original source codes.

Staging not much different from development mode. We not created script for it, but if you really need it, we will create it additionally

Script for installation "install.sh"
You need to launch it from user. It will prompt for sudo pass when needed during installation.
You can launch it with -h and get help.
This script will get domain name in parameters which you need, if you will not specify it will create on "mycartodb.com"
With flag -d you can set directory of installation. By default it will be home directory, but there are much of trash files.
So the best here to set something like -d myCartoDB


Note that with installation description on github/CartoDB we have found problems, even without production mode

For Node.js required another repository
ppa:cartodb/nodejs-010 instead of mentioned ppa:cartodb/nodejs

Some other in descriptiion from CartoDB's github, there not all written right. We fixed much of small problems. You can will see it in the installation sript.

Redis server
We install it from sources, not from apt-get repository
It should be installed and launched as system process, because if trying to do like it was mentioned on CartoDB's github and launch from user, it will be not with some restrictions, for example limit for less memory and limit for connections
Also we changed one line in core:
vm.overcommit_memory = 1 in etc/sysctl.conf

Varnish
During installation it is required that it installs from CartoDB repository and not from default repository.
So with apt-get it will not work, as by default it installs the latest version, which will not work. So it required to make PIN in aptget, you can see this in script on 292 line;
It required to change configs after installation so it will not require secret key for connection. In config it was written
Key maybe and useful, but as Varnish will work anyways and key will not be used we block it. In script you will see it after "PIN"

It has two Node.js servers:
CartoDB SQL API
CartoDB Maps API (Windshaft-cartodb)
Both these server has configs for each mode.
There was too much changes, you will see in scripts with regular sentences.
If you will have problems with that we will make additional descriptiion
Especially we made much of changes to production mode configuration.

Main config CartoDB
app_config.yml
This file also have much of script changes, as default file not working either.
Be cautive, in the config file spaces count. It works similar to Python language, and it is first such config file we ever see.
So amount of spaces will count. You should take caution about this, as it can provoke some problems

For production mode it requires in app_config.yml section of "graphite:" there will be "graphite_public:", but it will be not enough, you will need just "graphite:" additionally, there you need to pick host and port for this system, and if graphite-web we not installed, just picked our host and port, without this cartodb will not create user tables in production mode.

And error we just found only today.
Package cartpdb-postgresql will be installed from Github, and version of this package hardwritten in source codes of CartoDB and marked from developers with some sort of "//TODO Need to be changed in the near time"
In some moments version will be equivalent with version which written there, in this case all will work, but when they change the version of cartodb-postgresql, like it was yesterday, then CartoDB during trying of creation Databases for users will write something like
extension "cartodb" has no update path from version "unpackaged" to version "0.4.0"
So yesterday version was 0.4.0, and today it is 0.4.1
Same problem you can face with if try installation from another branch, for example release, as in the sources it was hardwritten.
Our script now catch this error, automatically updates installed version and fixes source code. It should be 428 line

Another big problem with source codes, where we made our patch
When production mode activated for CartoDB, then two servers, CartoDB SQL API and CartoDB Maps API, CartoDB thinks that in main config will be set protocol for messaging with server https, but there is not, there written http as supposed for development mode. So config file is one for all modes, but for production mode https is required.
Если прописан http, то CartoDB не работает, без никаких ошибок. Просто не запускается. Даже не пытается подключится к этим серверам. Если же прописать в конфиге протокол https, то CartoDB пытается с ними соединиться по протоколу https, но эти два сервера не умеют принимать эти соединения, по крайней мере в том виде в котором эти сервера сейчас находятся (последняя мастер ветка).
And there is no any mentions about SSL connections in any configs or any official documentation! Maybe it should work somehow, but then it will require to research all source codes perfectly to understand how it will work. But we cannot do it in small amount of time.
So we use the patch, to change source codes, to not use SSL connection in prodcution mode, so it will work in http
In common configuration these two servers will work on the same physical server with CartoDB, so there is no point making protected connection among them. It makes sense only if they will work on separate servers.
For this purpose we made file "patch1", it should be placed with install.sh in the same directory

And one more thing about production mode.
Instead of "bundle install" command, which used for production mode, it should use "bundle install --deployment" which will make everything differently

If development mode.
Development mode use web server "Thin Web" and port 3000
But for production mode it requires full web server, nginx or apache with Phusion Passenger module for making Ruby applications (as great part of CartoDB sources are written in Ruby)
Production mode was planned to work under https connection. Patch that we created, it partially takes away requirement to use https, but in CartoDB source codes you will see too much https words in links and such, to find all of these it will require to make many changes in sources manually. So in this place we pushed to use https, which will require web-certificate. At the moment script will generate two seld-signed certificates. All working as planned, but client-side browser will ask about acceptance of these certificates. After you buy real certificates you can just change these two files.
In production mode will be required root for launch. Script which makes start/stop/restart cartodb.sh can be launched from user and it should be placed in CartoDB base folder. But it will will require sudo password in production mode, as in production mode you need ports 80 and 443, and to use these ports we need root privileges, so that is why web server will start with root, but after that web server will work without priveledged user rights. So root we need only for launch to open specific ports, to make bind on them.
In development mode, there 3000 port and so no special privileges required
And in production all will work without rights, only nginx during the startup.

Domain name
You should have common domain name to use CartoDB, as it will not work without it, as all source codes were written specially to count on that. Domain should have look like *.mycartodb.com, so it should take any sub-domains, as for each user CartoDB creates sub-domain, for example user.mycartodb.com
If you have domain name, you simply need to add zone, which should allow all sub-domains. If you have no domain name, we created for tests script, which will set DNS server on your Amazon server. Script will automatically detect public IP and will write it inside of configuration with your domain name of your choice, which you will set in this script. So it will be sort of emulation of external ("real") domain name.
If on Amazon instance it change its public IP after each start and stop, you will be required to relaunch script with DNS setup
On your local machine you should set this Amazon server as your primary DNS server, so you will get list of domains from there.
Also, on Amazon instance should be opened port 53 for UDP
And following ports should be opened also:
8080 - For CartoDB SQL API
8181 - CartoDB Maps API (Windshaft-cartodb)
80 - For web server in http
443 - For web server in https
Also, in the end of setdns.sh script in comments you will se what required to do if Amazon have no static IP

As for AMI.
We can't provide image, as full CartoDB does not install on micro instance, as it lacks of memory. So it require priced plan.

You should use Ubuntu 12 without any additional installed services.

If you will have some errors in these scripts we will fix it.

Scripts removed the restriction on the table, the size, make a server dedicated and not free, like on CartoDB.com

Script adduser.sh will ask what to do with CartoDB user and how to set it. It should be launched with user level rights. And it should be launched from CartoDB base level directory

We have not made functionality of external services, there require API keys of that services, so it will take much of things not related to CartoDB and we were not ready for this. We can do it, but not in the current project, as it will take too much time.

As for staging mode, please specify if you need it, as it have no much point in it. It is very similar to development mode. If you have a strong need in this mode we will create a script.

And note about problem, in the source codes very often hard written domain CartoDB.com and this can't be changed simply without config files. For example registration module, if you try to register, you will be sent to cartodb.com site. If you ever need these functionality we can write registration and any other things with our developers, but not in the form of current project.

Scripts can be wet, not finished. It took much of hard work, but some things can be broked, as we tried to do as fast for you. If you will face with any problem we are ready to help, but without addition of any new features and possibilities in the current project.

In all scripts you can launch help with key -h or --help so you will get some info

adduser.sh install.sh cartodb.sh you should always launch from user rights, if it will require some rights it will ask you for password
Only setdns.sh should be launched with root rights (sudo setdns.sh)

Standard ways of CartoDB not give us ways to clearly delete user. We not made script for that. We can delete it from base, but not sure if it will not break any dependencies.

If you want apache server, we can help with installation of apache instead of nginx, nginx was chosen as it is more powerful

If you will have problems with domain, we will help you

Finally, if you will want to continue on improvements we are ready to continue work, but only with hour paid job.
Further job will be easier, as we know much parts of source codes. It was hard at the start, as overally CartoDB project have no actual documentation and all is unknown. Now we can imagine where we should look when something works not how it supposed to.
And to create as cool as original CartoDB.com we will require much more time

We made all we could, but this project was too complicated, which we not wait for. Hope it helps

