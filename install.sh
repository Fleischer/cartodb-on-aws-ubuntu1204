#!/bin/bash

PROGNAME=${0##*/}

TERM_WIDTH=$(stty size | cut -d" " -f2)

message()
{
    printf "\n%${TERM_WIDTH}s\n" | sed "s/ /#/g"
    MESSAGE="$PROGNAME: $*"
    echo "$MESSAGE" | fold -s -w ${TERM_WIDTH:-80}
    printf "%${TERM_WIDTH}s\n\n" | sed "s/ /#/g"
}

error_message()
{
    MESSAGE="$PROGNAME: $*"
    echo "$MESSAGE" | fold -s -w ${TERM_WIDTH:-80} >&2
}

error_exit()
{
    error_message "$*"
    exit 1
}

do_help()
{
  printf "
Usage:
    $PROGNAME [options] [DOMAIN]

Options:

    -d, --install-dir DIR
        set installation subdirectory inside user home dir

    -h, --help
        output help message

"
}

SHORTOPTS="hd:"
LONGOPTS="help,install-dir:"

TMP=$(mktemp)
ARGS=$(getopt -s bash --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" 2>"$TMP")
ERR=$(cat "$TMP")
rm "$TMP"

if [ -n "$ERR" ]; then echo; echo "$ERR"; do_help; exit 1; fi

eval set -- "$ARGS"

while true
do
    case "$1" in
        -h|--help)
            do_help
            exit 0
            ;;
        -d|--install-dir)
            case "$2" in
                "") shift 2 ;;
                *) INST_DIR="$2"; shift 2 ;;
            esac
            ;;
        --)
            shift
            break
            ;;
        *)
            error_exit 'Internal error!'
            ;;
    esac
done


if [ -n "$INST_DIR" ]; then INST_DIR=$HOME/"$INST_DIR"; else INST_DIR=$HOME; fi

SITE_DOMAIN=$1
if [ -z "$SITE_DOMAIN" ]; then SITE_DOMAIN=mycartodb.com; fi

mkdir -p "$INST_DIR"
mv patch1 "$INST_DIR"/ 2>/dev/null

sudo apt-get update
sudo apt-get -y install python-software-properties

##########################
message 'Add CartoDB PPAs'
##########################

sudo add-apt-repository -y ppa:cartodb/base
sudo add-apt-repository -y ppa:cartodb/gis
sudo add-apt-repository -y ppa:cartodb/mapnik
sudo add-apt-repository -y ppa:cartodb/nodejs-010
sudo add-apt-repository -y ppa:cartodb/redis
sudo add-apt-repository -y ppa:cartodb/postgresql-9.3
sudo add-apt-repository -y ppa:cartodb/varnish

sudo apt-get update

############################
message 'Get CartoDB source'
############################

sudo apt-get -y install git

cd "$INST_DIR"

git clone --recursive https://github.com/CartoDB/cartodb.git
cd cartodb
git checkout master

###################################
message 'Install some dependencies'
###################################

sudo apt-get -y install unp zip

#############################
message 'Install build tools'
#############################

sudo apt-get -y install build-essential

######################
message 'Install GEOS'
######################

sudo apt-get -y install libgeos-c1 libgeos-dev

######################
message 'Install GDAL'
######################

sudo apt-get -y install gdal-bin libgdal1-dev

########################
message 'Install JSON-C'
########################

sudo apt-get -y install libjson0 python-simplejson libjson0-dev

######################
message 'Install PROJ'
######################

sudo apt-get -y install proj-bin proj-data libproj-dev

############################
message 'Install PostgreSQL'
############################

sudo apt-get -y install postgresql-9.3 postgresql-client-9.3 \
    postgresql-contrib-9.3 postgresql-server-dev-9.3
sudo apt-get -y install postgresql-plpython-9.3

PG_HBA_SH="$INST_DIR"/pg_hba.sh
echo '#!/bin/bash

PG_HBA_CONF=/etc/postgresql/9.3/main/pg_hba.conf
awk '\''{if (NF == 5 && $1 == "host" && $2 == "all") $0 = substr($0, 1, \
    length($0) - length($5)) "trust"; if (NF == 4 && $1 == "local" && $2 == "all") \
    $0 = substr($0, 1, length($0) - length($4)) "trust" } 1'\'' $PG_HBA_CONF \
    > $PG_HBA_CONF.tmp && cat $PG_HBA_CONF.tmp > $PG_HBA_CONF && rm $PG_HBA_CONF.tmp
' > $PG_HBA_SH

chmod +x $PG_HBA_SH && sudo $PG_HBA_SH && rm $PG_HBA_SH

sudo /etc/init.d/postgresql restart

#########################
message 'Install PostGIS'
#########################

cd /usr/local/src
sudo wget http://download.osgeo.org/postgis/source/postgis-2.1.4.tar.gz
sudo tar -xzf postgis-2.1.4.tar.gz
cd postgis-2.1.4
sudo ./configure
sudo make
sudo make install

TMPL_PG_FILE="$INST_DIR"/template_postgis.sh
echo '#!/bin/bash

createdb -E UTF8 template_postgis
createlang -d template_postgis plpgsql
psql -d postgres -c \
 "UPDATE pg_database SET datistemplate='\''true'\'' WHERE datname='\''template_postgis'\''"
psql -d template_postgis -c "CREATE EXTENSION postgis;"
psql -d template_postgis -c "CREATE EXTENSION postgis_topology;"
psql -d template_postgis -c "CREATE EXTENSION fuzzystrmatch;"
psql -d template_postgis -c "CREATE EXTENSION postgis_tiger_geocoder;"
psql -d template_postgis -c "GRANT ALL ON geometry_columns TO PUBLIC;"
psql -d template_postgis -c "GRANT ALL ON spatial_ref_sys TO PUBLIC;"
' > $TMPL_PG_FILE

chmod +x $TMPL_PG_FILE && sudo su -s /bin/bash - postgres -c $TMPL_PG_FILE && rm $TMPL_PG_FILE

####################################
message 'Install cartodb-postgresql'
####################################

cd "$INST_DIR"

git clone https://github.com/CartoDB/pg_schema_triggers.git
cd pg_schema_triggers
sudo make install

echo "shared_preload_libraries = 'schema_triggers.so'" | \
    sudo tee -a /etc/postgresql/9.3/main/postgresql.conf

sudo /etc/init.d/postgresql restart

cd ..

git clone https://github.com/CartoDB/cartodb-postgresql.git
git checkout -b ver030 0.3.0
cd cartodb-postgresql
sudo make install

######################
message 'Install Ruby'
######################

sudo apt-get -y install curl

cd "$INST_DIR"

\curl -sSL https://get.rvm.io | bash -s stable
source $HOME/.rvm/scripts/rvm

rvm install 1.9.3

gem install bundler

#########################
message 'Install Node.js'
#########################

sudo apt-get -y install nodejs

#######################
message 'Install Redis'
#######################

echo "vm.overcommit_memory = 1" | sudo tee -a /etc/sysctl.conf
sudo sysctl vm.overcommit_memory=1

cd /usr/local/src
sudo wget http://download.redis.io/releases/redis-2.8.17.tar.gz
sudo tar -xzf redis-2.8.17.tar.gz
cd redis-2.8.17
sudo make
sudo make install
cd utils
echo | sudo ./install_server.sh

#####################################
message 'Install Python dependencies'
#####################################

sudo apt-get -y install python2.7-dev
sudo apt-get -y install python-setuptools
sudo easy_install pip

cd "$INST_DIR"/cartodb

PY_REQ_FILE=py_req.sh
echo '#!/bin/bash

export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

pip install --no-use-wheel -r python_requirements.txt
' > $PY_REQ_FILE

chmod +x $PY_REQ_FILE && sudo ./$PY_REQ_FILE && rm $PY_REQ_FILE

#########################
message 'Install Varnish'
#########################

cd "$INST_DIR"

VARNISH_SH=varnish_apt.sh
echo '#!/bin/bash

echo "Package: varnish
Pin: release o=LP-PPA-cartodb-varnish
Pin-Priority: 999
" > /etc/apt/preferences.d/varnish
' > $VARNISH_SH

chmod +x $VARNISH_SH && sudo ./$VARNISH_SH && rm $VARNISH_SH

sudo apt-get -y install varnish

VARNISH_SH=varnish_def.sh
echo '#!/bin/bash

VARNISH_DEF=/etc/default/varnish
sed -r '\''s/^\s*-S\s*\/etc\/varnish\/secret//'\'' $VARNISH_DEF \
    > $VARNISH_DEF.tmp && cat $VARNISH_DEF.tmp > $VARNISH_DEF && rm $VARNISH_DEF.tmp
' > $VARNISH_SH

chmod +x $VARNISH_SH && sudo ./$VARNISH_SH && rm $VARNISH_SH

sudo /etc/init.d/varnish restart

########################
message 'Install Mapnik'
########################

sudo apt-get -y install libmapnik-dev python-mapnik mapnik-utils

#################################
message 'Install CartoDB SQL API'
#################################

cd "$INST_DIR"

git clone git://github.com/CartoDB/CartoDB-SQL-API.git
cd CartoDB-SQL-API
git checkout master
npm install

cd config/environments
cp development.js.example development.js
cp production.js.example production.js
cp staging.js.example staging.js

CFG_FILE=development.js
sed -r "s/^(\s*module.exports.user_from_host\s*=\s*')(.*)('.*)/\1^(.*)\
\\\\\\\\.${SITE_DOMAIN//./\\\\\\\\.}\3/" "$CFG_FILE" | \
sed -r "s/^(\s*module.exports.node_host\s*=\s*')(.*)('.*)/\1\3/" > \
    "$CFG_FILE".tmp && cat "$CFG_FILE".tmp > "$CFG_FILE" && rm "$CFG_FILE".tmp

CFG_FILE=production.js
sed -r "s/^(\s*module.exports.user_from_host\s*=\s*')(.*)('.*)/\1^(.*)\
\\\\\\\\.${SITE_DOMAIN//./\\\\\\\\.}\3/" "$CFG_FILE" | \
sed -r "s/^(\s*module.exports.node_host\s*=\s*')(.*)('.*)/\1\3/" | \
sed -r "s/^(\s*module.exports.db_port.*')(6432)(.*)/\15432\3/" > \
    "$CFG_FILE".tmp && cat "$CFG_FILE".tmp > "$CFG_FILE" && rm "$CFG_FILE".tmp

###################################
message 'Install Windshaft-cartodb'
###################################

cd "$INST_DIR"

git clone git://github.com/CartoDB/Windshaft-cartodb.git
cd Windshaft-cartodb
git checkout master
npm install

cd config/environments
cp development.js.example development.js
cp production.js.example production.js
cp staging.js.example staging.js

CFG_FILE=development.js
sed -r "s/^(\s*,user_from_host:\s*')(.*)('.*)/\1^(.*)\
\\\\\\\\.${SITE_DOMAIN//./\\\\\\\\.}\3/" "$CFG_FILE" | \
sed -r "s/^(\s*,host:\s*')(.*)('.*)/\1\3/" | \
sed -r "s/^(\s*domain:\s*')(.+)('.*)/\1$SITE_DOMAIN\3/" > \
    "$CFG_FILE".tmp && cat "$CFG_FILE".tmp > "$CFG_FILE" && rm "$CFG_FILE".tmp

CFG_FILE=production.js
sed -r "s/^(\s*,user_from_host:\s*')(.*)('.*)/\1^(.*)\
\\\\\\\\.${SITE_DOMAIN//./\\\\\\\\.}\3/" "$CFG_FILE" | \
sed -r "s/^(\s*,host:\s*')(.*)('.*)/\1\3/" | \
sed -r "s/^(\s*domain:\s*')(.+)('.*)/\1$SITE_DOMAIN\3/" | \
sed -r "s/^(\s*port:.*)(6432)(.*)/\15432\3/" | \
sed -r "s/^(\s*protocol:.*)(https)(.*)/\1http\3/" | \
sed -r "s/^(\s*version:.*)(v2)(.*)/\1v1\3/" | \
sed -r "s/^(\s*cache_basedir:\s*')(.+)('.*)/\1$(printf "${INST_DIR//\//\\/}")\\/tile_assets\3/" > \
    "$CFG_FILE".tmp && cat "$CFG_FILE".tmp > "$CFG_FILE" && rm "$CFG_FILE".tmp

cd ../..

mkdir logs
mkdir "$INST_DIR"/tile_assets

#############################
message 'Install ImageMagick'
#############################

sudo apt-get -y install imagemagick

#############################
message 'Setup CartoDB'
#############################

source $HOME/.rvm/scripts/rvm

cd "$INST_DIR"

cd cartodb/config

cp app_config.yml.sample app_config.yml
cp database.yml.sample database.yml

CFG_FILE=app_config.yml
sed -r "s/^(\s*session_domain:\s*['\"])(.*)(['\"].*)/\1.$SITE_DOMAIN\3/" "$CFG_FILE" | \
sed -r "s/^(\s*domain:\s*['\"])(.+)(['\"].*)/\1$SITE_DOMAIN\3/" | \
sed -r "s/^(\s*tiler_domain:\s*['\"])(.+)(['\"].*)/\1$SITE_DOMAIN\3/" | \
sed -r "s/^(\s*tiler_port:\s*['\"])(.+)(['\"].*)/\18181\3/" | \
sed -r "s/^(\s*sql_domain:\s*['\"])(.+)(['\"].*)/\1$SITE_DOMAIN\3/" | \
sed -r "s/^(\s*sql_port:\s*['\"])(.+)(['\"].*)/\18080\3/" | \
sed -r "/^production:/{n;s/^(\s*)(<<:\s*\*defaults)/\1\2\n\
\1graphite_public:\n\1\1host:  '$SITE_DOMAIN'\n\1\1port:  '2003'\n\
\1graphite:\n\1\1host:  '$SITE_DOMAIN'\n\1\1port:  '2003'\n\
\1enforce_non_empty_layer_css: false/}" > \
    "$CFG_FILE".tmp && cat "$CFG_FILE".tmp > "$CFG_FILE" && rm "$CFG_FILE".tmp

cd ..

### CartoDB source Patch 1

patch app/controllers/application_controller.rb ../patch1

### CartoDB source Patch 2

CDB_EXT_VER=$(grep -E "default_version\s*=.*" `pg_config --sharedir`/extension/cartodb.control | \
    sed -r "s/^(default_version\s*=\s*')(.*)('.*)/\2/")
SRC_FILE=app/models/user.rb
sed -r "s/^(\s*cdb_extension_target_version\s*=\s*')(.*)('.*)/\1$CDB_EXT_VER\3/" "$SRC_FILE" > \
    "$SRC_FILE".tmp && cat "$SRC_FILE".tmp > "$SRC_FILE" && rm "$SRC_FILE".tmp

message 'Setup development mode'

export RAILS_ENV=development

rvm use 1.9.3@cartodb-dev --create
rvm use 1.9.3@cartodb-dev --default
bundle install

DEF_DB_USER="user1"
DEF_DB_USER_PASSWD="user1"
DEF_DB_USER_EMAIL="user1@$SITE_DOMAIN"

read -p "Enter initial user for development database: [$DEF_DB_USER] " DB_USER
read -p "Enter user password for development database: [$DEF_DB_USER_PASSWD] " DB_USER_PASSWD
read -p "Enter user email for development database: [$DEF_DB_USER_EMAIL] " DB_USER_EMAIL
if [ -z "$DB_USER" ]; then DB_USER="$DEF_DB_USER"; fi
if [ -z "$DB_USER_PASSWD" ]; then DB_USER_PASSWD="$DEF_DB_USER_PASSWD"; fi
if [ -z "$DB_USER_EMAIL" ]; then DB_USER_EMAIL="$DEF_DB_USER_EMAIL"; fi

message 'Creating development database ...'

bundle exec rake cartodb:db:setup SUBDOMAIN="$DB_USER" PASSWORD="$DB_USER_PASSWD" \
    EMAIL="$DB_USER_EMAIL"

read -p "Enter user quota (Mb): " DB_USER_QUOTA
if [ -n "$DB_USER_QUOTA" ]
then
    echo "--- Updating quota to $DB_USER_QUOTA MB ---"
    bundle exec rake cartodb:db:set_user_quota["$DB_USER","$DB_USER_QUOTA"]
fi

read -p "Allow unlimited tables creation? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing unlimited tables creation ---"
    bundle exec rake cartodb:db:set_unlimited_table_quota["$DB_USER"]
fi

read -p "Allow user to create private tables in addition to public? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing private tables creation ---"
    bundle exec rake cartodb:db:set_user_private_tables_enabled["$DB_USER",'true']
fi

read -p "Set the account type 'DEDICATED'? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Setting cartodb account type ---"
    bundle exec rake cartodb:db:set_user_account_type["$DB_USER",'[DEDICATED]']
fi

message 'Setup production mode'

export RAILS_ENV=production

rvm use 1.9.3@cartodb --create
rvm use 1.9.3@cartodb --default
bundle install --deployment

gem install passenger

export rvmsudo_secure_path=1
echo | rvmsudo passenger-install-nginx-module

read -p "Enter initial user for production database: [$DEF_DB_USER] " DB_USER
read -p "Enter user password for production database: [$DEF_DB_USER_PASSWD] " DB_USER_PASSWD
read -p "Enter user email for production database: [$DEF_DB_USER_EMAIL] " DB_USER_EMAIL
if [ -z "$DB_USER" ]; then DB_USER="$DEF_DB_USER"; fi
if [ -z "$DB_USER_PASSWD" ]; then DB_USER_PASSWD="$DEF_DB_USER_PASSWD"; fi
if [ -z "$DB_USER_EMAIL" ]; then DB_USER_EMAIL="$DEF_DB_USER_EMAIL"; fi

message 'Creating production database ...'

bundle exec rake cartodb:db:setup SUBDOMAIN="$DB_USER" PASSWORD="$DB_USER_PASSWD" \
    EMAIL="$DB_USER_EMAIL"

read -p "Enter user quota (Mb): " DB_USER_QUOTA
if [ -n "$DB_USER_QUOTA" ]
then
    echo "--- Updating quota to $DB_USER_QUOTA MB ---"
    bundle exec rake cartodb:db:set_user_quota["$DB_USER","$DB_USER_QUOTA"]
fi

read -p "Allow unlimited tables creation? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing unlimited tables creation ---"
    bundle exec rake cartodb:db:set_unlimited_table_quota["$DB_USER"]
fi

read -p "Allow user to create private tables in addition to public? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing private tables creation ---"
    bundle exec rake cartodb:db:set_user_private_tables_enabled["$DB_USER",'true']
fi

read -p "Set the account type 'DEDICATED'? (yes/no): " DB_USER_ANSWER
if [ "${DB_USER_ANSWER,,}" == 'y' -o "${DB_USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Setting cartodb account type ---"
    bundle exec rake cartodb:db:set_user_account_type["$DB_USER",'[DEDICATED]']
fi

cd ..

mkdir .ssl && cd .ssl

openssl req -new -x509 -days 3650 -sha1 -newkey rsa:2048 -nodes -keyout \
cartodb.key -out cartodb.crt -subj \
"/C=US/ST=Some\ region/L=City/O=Comp\ Ltd./OU=IT/CN=$SITE_DOMAIN"

cd ..

sudo useradd -r -s /usr/sbin/nologin -d /opt/nginx nginx

CFG_FILE=/opt/nginx/conf/nginx.conf
sed -r "/^\s*listen\s*80.*/{n;s/^\s*server_name\s*localhost.*/        listen       443 ssl;\n\
        server_name  *.$SITE_DOMAIN;\n\n\
        ssl_certificate      $(printf "${INST_DIR//\//\\/}")\\/.ssl\\/cartodb.crt;\n\
        ssl_certificate_key  $(printf "${INST_DIR//\//\\/}")\\/.ssl\\/cartodb.key;\n\n\
        ssl_session_cache shared:SSL:1m;\n\
        ssl_session_timeout  5m;\n\n\
        root   $(printf "${INST_DIR//\//\\/}")\\/cartodb\\/public;\n\
        passenger_enabled on;\n\
        rails_env production;\n\n\
        #access_log  logs\\/host.access.log  main;\n\
    \\}\n\n\
    server {\n\
        listen       80;\n\
        server_name  localhost;/}" "$CFG_FILE" | \
sed -r "s/^.*#user\s*nobody.*/user  nginx;/" | sudo tee "$CFG_FILE".tmp 1>/dev/null
cat "$CFG_FILE".tmp | sudo tee "$CFG_FILE"
sudo rm "$CFG_FILE".tmp

sudo mkdir /var/lib/cartodb
sudo chown $(id -u):$(id -g) /var/lib/cartodb

