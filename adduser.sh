#!/bin/bash

PROGNAME=${0##*/}

do_help()
{
  printf "
Usage:
    $PROGNAME [options]

Options:

    -m, --db-mode MODE
        set database mode, 'p' (production) 'd' (development), default development

    -h, --help
        output help message

"
}

SHORTOPTS="hm:"
LONGOPTS="help,db-mode:"

TMP=$(mktemp)
ARGS=$(getopt -s bash --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $PROGNAME -- "$@" 2>"$TMP")
ERR=$(cat "$TMP")
rm "$TMP"

if [ -n "$ERR" ]; then echo; echo "$ERR"; do_help; exit 1; fi

eval set -- "$ARGS"

while true
do
    case "$1" in
        -h|--help)
            do_help
            exit 0
            ;;
        -m|--db-mode)
            case "$2" in
                "") shift 2 ;;
                *) DB_MODE="$2"; shift 2 ;;
            esac
            ;;
        --)
            shift
            break
            ;;
        *)
            error_exit 'Internal error!'
            ;;
    esac
done

source $HOME/.rvm/scripts/rvm

if [ "${DB_MODE,,}" == 'p' ]
then
  DB_MODE=production
else
  DB_MODE=development
fi

if [ "$DB_MODE" == 'production' ]
then
    export RAILS_ENV=production
    rvm use 1.9.3@cartodb --default
else
    export RAILS_ENV=development
    rvm use 1.9.3@cartodb-dev --default
fi

read -p "Enter user name: " USER_NAME
read -p "Enter user password: " USER_PASSWD
read -p "Enter user email: " USER_EMAIL

echo "--- Creating user ---"
bundle exec rake cartodb:db:create_user SUBDOMAIN="$USER_NAME" \
    PASSWORD="$USER_PASSWD" EMAIL="$USER_EMAIL"

if [ $? -ne 0 ]; then exit 1; fi

read -p "Enter user quota (Mb): " USER_QUOTA
if [ -n "$USER_QUOTA" ]
then
    echo "--- Updating quota to $USER_QUOTA MB ---"
    bundle exec rake cartodb:db:set_user_quota["$USER_NAME","$USER_QUOTA"]
    if [ $? -ne 0 ]; then exit 1; fi
fi

read -p "Allow unlimited tables creation? (yes/no): " USER_ANSWER
if [ "${USER_ANSWER,,}" == 'y' -o "${USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing unlimited tables creation ---"
    bundle exec rake cartodb:db:set_unlimited_table_quota["$USER_NAME"]
    if [ $? -ne 0 ]; then exit 1; fi
fi

read -p "Allow user to create private tables in addition to public? (yes/no): " USER_ANSWER
if [ "${USER_ANSWER,,}" == 'y' -o "${USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Allowing private tables creation ---"
    bundle exec rake cartodb:db:set_user_private_tables_enabled["$USER_NAME",'true']
    if [ $? -ne 0 ]; then exit 1; fi
fi

read -p "Set the account type 'DEDICATED'? (yes/no): " USER_ANSWER
if [ "${USER_ANSWER,,}" == 'y' -o "${USER_ANSWER,,}" == 'yes' ]
then
    echo "--- Setting cartodb account type ---"
    bundle exec rake cartodb:db:set_user_account_type["$USER_NAME",'[DEDICATED]']
    if [ $? -ne 0 ]; then exit 1; fi
fi
