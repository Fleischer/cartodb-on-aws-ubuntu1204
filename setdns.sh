#!/bin/bash

PROGNAME=${0##*/}

TERM_WIDTH=$(stty size | cut -d" " -f2)

message()
{
    printf "\n%${TERM_WIDTH}s\n" | sed "s/ /#/g"
    MESSAGE="$PROGNAME: $*"
    echo "$MESSAGE" | fold -s -w ${TERM_WIDTH:-80}
    printf "%${TERM_WIDTH}s\n\n" | sed "s/ /#/g"
}

error_message()
{
    MESSAGE="$PROGNAME: $*"
    echo "$MESSAGE" | fold -s -w ${TERM_WIDTH:-80} >&2
}

error_exit()
{
    error_message "$*"
    exit 1
}

do_help()
{
  printf "
Usage:
    $PROGNAME [options] [DOMAIN]

Options:

    -h, --help
        output help message

"
}

if [ -n "$1" ]; then if [ "$1" == '-h' -o "$1" == '--help' ]; then do_help; exit 0; fi; fi

if [ $(id -u) -ne "0" ]; then error_exit "This script must be run as root!"; fi

SITE_DOMAIN=$1
if [ -z "$SITE_DOMAIN" ]; then SITE_DOMAIN=mycartodb.com; fi

PKG_BIND9=$(dpkg --get-selections | grep -E '^bind9\s*install')

if [ -z "$PKG_BIND9" ]
then
    apt-get update
    apt-get -y install bind9
fi

PUBLIC_IP=$(wget http://ipecho.net/plain -O - -q)

echo -e "\$TTL 10800 ; 3 hours
@ SOA $SITE_DOMAIN. root.$SITE_DOMAIN. (
    2014102101 ; serial
    86400      ; refresh (1 day)
    43200      ; retry (12 hours)
    604800     ; expire (1 week)
    10800      ; minimum (3 hours)
)
    NS\t$SITE_DOMAIN.

\t\tA\t$PUBLIC_IP
*.$SITE_DOMAIN.\tA\t$PUBLIC_IP
" > /etc/bind/db.${SITE_DOMAIN/./-}

A1=$(echo $PUBLIC_IP | awk -F'.' '{ print $1; }')
A2=$(echo $PUBLIC_IP | awk -F'.' '{ print $2; }')
A3=$(echo $PUBLIC_IP | awk -F'.' '{ print $3; }')
A4=$(echo $PUBLIC_IP | awk -F'.' '{ print $4; }')

echo -e "\$TTL 10800 ; 3 hours
@ SOA $SITE_DOMAIN. root.$SITE_DOMAIN. (
    2014102101 ; serial
    86400      ; refresh (1 day)
    43200      ; retry (12 hours)
    604800     ; expire (1 week)
    10800      ; minimum (3 hours)
)
    NS\t$SITE_DOMAIN.

\t\tPTR\t$SITE_DOMAIN.
" > /etc/bind/db.${SITE_DOMAIN/./-}-ptr

echo -e "
zone \"$SITE_DOMAIN\" {
    type master;
    file \"/etc/bind/db.${SITE_DOMAIN/./-}\";
};
zone \"$A4.$A3.$A2.$A1.in-addr.arpa\" {
    type master;
    file \"/etc/bind/db.${SITE_DOMAIN/./-}-ptr\";
};
" > /etc/bind/named.conf.local

echo -e "
options {
\tdirectory \"/var/cache/bind\";
\tdnssec-validation auto;
\tauth-nxdomain no;
\tlisten-on { any; };
\tallow-query { any; };
};
" > /etc/bind/named.conf.options

/etc/init.d/bind9 restart

#####
##### if dhcp run command
# echo "supersede domain-name-servers 127.0.0.1;" | sudo tee -a /etc/dchp/dhclient.conf
##### if static ip run command
# echo "dns-nameservers 127.0.0.1" | sudo tee -a /etc/network/interfaces.d/eth0.cfg
##### restart network
# sudo /etc/init.d/networking restart
##### add domain in /etc/hostname
#####
